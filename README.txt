Flir - Convert your Drupal text to generated font-based images using the FLIR project
Copyright (C) 2008  The Pennsylvania State University

Bryan Ollendyke
bto108@psu.edu

Keith D. Bailey
kdb163@psu.edu

12 Borland
University Park, PA 16802

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

INSTALLATION
*Place in your install directory (usually sites/all/modules/)
*Activate the module
*Goto admin/settings/flir
*It will create directories, copy the fonts from sites/all/modules/flir/flir/fonts into files/flir/fonts (or add your own fonts to that directory!)
*Refresh the admin/settings/flir page, you're fonts will appear!
**See jQuery documentation if you need help figuring out how to formular tags to select
**Select your font, submit and watch it change everything automatically!!